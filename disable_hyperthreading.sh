#!/bin/bash

ht_cores="$(\
    grep -H . /sys/devices/system/cpu/cpu*/topology/thread_siblings_list | \
    sort -n -t ',' -k 2 -u | \
    sed 's/^.*siblings_list:\([0-9]\+\),\([0-9]\+\)$/\2/g')"

for core in $ht_cores; do
    echo "Disabling HT core $core"
    echo 0 | sudo tee "/sys/devices/system/cpu/cpu${core}/online"
done
