#include <papi.h>
#include <stdio.h>

volatile int _result = 0; // Make sure this isn't optimized out

int fibo(int n) { // Inefficient fibo -- shoud take a few cycles
    if(n <= 1)
        return 1;
    return fibo(n-2) + fibo(n-1);
}

int main(void) {
    int retval;
    retval=PAPI_library_init(PAPI_VER_CURRENT);
    if (retval!=PAPI_VER_CURRENT) {
        fprintf(stderr,"Error initializing PAPI! %s\n",
                PAPI_strerror(retval));
        return 2;
    }

    int eventset=PAPI_NULL;
    retval=PAPI_create_eventset(&eventset);
    if (retval!=PAPI_OK) {
        fprintf(stderr,"Error creating eventset! %s\n",
                PAPI_strerror(retval));
        return 2;
    }

    retval=PAPI_add_named_event(eventset,"PAPI_TOT_CYC");
    if (retval!=PAPI_OK) {
        fprintf(stderr,"Error adding PAPI_TOT_CYC: %s\n",
                PAPI_strerror(retval));
        return 1;
    }

    long long count = 0;

    PAPI_reset(eventset);
    retval=PAPI_start(eventset);
    if (retval!=PAPI_OK) {
        fprintf(stderr,"Error starting PAPI measure: %s\n",
                PAPI_strerror(retval));
        return 2;
    }

    _result = fibo(10);

    retval=PAPI_stop(eventset,&count);
    if (retval!=PAPI_OK) {
        fprintf(stderr,"Error stopping PAPI measure:  %s\n",
                PAPI_strerror(retval));
        return 2;
    }

    PAPI_cleanup_eventset(eventset);
    PAPI_destroy_eventset(&eventset);

    return 0;
}
