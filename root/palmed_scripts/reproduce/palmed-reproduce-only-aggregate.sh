#!/bin/bash

source $(dirname $(readlink -f "$0"))/_shared.sh

arch=$(find_arch $*) || exit 1

cd $HOME/palmed/
init_state_db "$arch"
init_state_checkpoints "$arch"
init_state_mapping "$arch"
init_state_eval_bench "$arch"
aggregate_results
