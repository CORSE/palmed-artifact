#!/bin/bash
# This file only defines functions, it is **not meant** to be run directly.

_BOLD="\e[1m"
_RED="\e[31m"
_GREEN="\e[32m"
_RESET="\e[0m"

function prompt_bool {
    echo -en "${_BOLD}> $1 [y/n]${_RESET} "
    while true; do
        read res
        case "$res" in
            "y") return 0;;
            "n") return 1;;
        esac
        echo -n "Please answer with 'y' or 'n': "
    done
}

function _log {
    >&2 echo -e "$_BOLD[$(date +%F-%T)]$1" $* "$_RESET"
}

function log_error {
    _log "$_RED" $*
}
function log_info {
    _log "$_GREEN" $*
}

function find_arch {
    # Takes the provided arch as $1.
    arch="$1"
    shift
    if [ -z "$arch" ]; then
        log_error "Missing parameter: architecture."
        exit 1
    fi
    case "$arch" in
        "SKX"|"ZEN");;
        *)
            log_error "Bad parameter: architecture. Valid values: SKX ZEN."
            exit 1
            ;;
    esac
    echo "$arch"
}

function is_db_clean {
    benchmark_count=$(psql palmed --tuples-only --no-align --quiet -c \
        'SELECT COUNT(*) FROM benchmark;')
    measure_count=$(psql palmed --tuples-only --no-align --quiet -c \
        'SELECT COUNT(*) FROM benchmark_measure;')
    return $( [ "$benchmark_count" -eq 0 ] && [ "$measure_count" -eq 0 ] )
}

function reset_state {
    if ! prompt_bool "You are about to reset all the performed experiments and lose the data. Are you sure?"; then
       >&2 echo "Aborting."
       exit 1
    fi

    pushd $HOME/palmed > /dev/null

    # Cleaning files
    rm -rf checkpoints out
    mkdir -p checkpoints out

    # Cleaning database
    psql --quiet palmed > /dev/null < $HOME/.initial_db.sql
    if ! is_db_clean ; then
        log_error "ERROR: the database wasn't correctly emptied. Aborting."
        exit 1
    fi

    log_info "All state correctly wiped."

    popd > /dev/null
}

function aggregate_results {
    pushd $HOME/palmed > /dev/null
    for result_file in ./out/eval-*.pickle; do
        suite=$(basename "$result_file" ".pickle")
        echo "======== RESULTS FOR $suite ========"
        # Generating tables
        $HOME/venv/bin/palmed-analyze-benchmarks --print-rms --tau "$result_file"

        # Generating heatmaps
        heatmaps_dir="$HOME/palmed/out/heatmaps"
        mkdir -p "$heatmaps_dir"
        $HOME/venv/bin/palmed-analyze-benchmarks \
            --heatmap "$heatmaps_dir/$suite" \
            "$result_file"
        log_info "Generated heatmaps under $heatmaps_dir."
        echo ""
    done
    popd > /dev/null
}

function evaluate_mapping_on_testsuite {
    # Arguments:
    # $1: testsuite, among those in /palmed_prebuilt/testsuites/
    # $2: arch (SKX, ZEN, …). Required for uops.

    testsuite=$1
    arch="${2^^}"
    suites_basedir="/palmed_prebuilt/testsuites/"
    suitedir="$suites_basedir$testsuite/"
    if ! [ -d "$suitedir" ]; then
        log_error "ERROR: No such testsuite: $testsuite." \
            "Available suites: $(ls -1 $suites_basedir)."
        exit 1
    fi

    mapping_file="$HOME/palmed/out/mapping.pipedream.localhost"
    if ! [ -f "$mapping_file" ] ; then
        log_error "ERROR: mapping '$mapping_file' not found." \
            "Did you run the previous steps, or correctly load prebuilt data?"
        exit 1
    fi

    additional_benchers=""
    case "$arch" in
        "SKX"|"SKL")
            additional_benchers="$additional_benchers --BUOPS --BIACA"
            ;;
    esac

    pushd $HOME/palmed > /dev/null
    log_info "Evaluating mapping on $testsuite..."
    $HOME/venv/bin/palmed-benchmarks \
        --tb-file "$suitedir/all_tbs" \
        --pickle "$HOME/palmed/out/eval-$testsuite.pickle" \
        --palmed-mapping "$mapping_file" \
        --arch "$arch" \
        --print-results \
        --Bnative --Bpalmed --BPMEvo --BLLVM_MCA $additional_benchers
    log_info "Finished evaluating mapping on $testsuite."
    popd > /dev/null
}

function evaluate_mapping {
    # Arguments:
    # $1: arch (SKX, ZEN, …). Required for uops.

    suites_basedir="/palmed_prebuilt/testsuites/"
    for suite in $suites_basedir/*; do
        if [ -d "$suite" ]; then
            evaluate_mapping_on_testsuite "$(basename $suite)" "$1"
        fi
    done
}

function generate_mapping {
    # Arguments:
    # $1: arch (SKX, ZEN, …).

    arch="$1"
    if [ -z "$arch" ]; then
        log_error "generate_mapping: required argument: arch."
        exit 1
    fi

    pushd $HOME/palmed > /dev/null
    mkdir -p checkpoints out
    log_info "Starting to generate Palmed mapping. This will take some time."
    $HOME/venv/bin/palmed --arch "$1"
    log_info "Finished generating Palmed mapping. Phew!"
    popd > /dev/null
}

function init_state_db {
    # Initialize the DB to some prebuilt data.
    # Arguments:
    # $1: arch (SKX, ZEN, …).

    case "$1" in
        "SKX")
            db="palmed_prefill_skx"
            ;;
        "ZEN")
            db="palmed_prefill_zen"
            ;;
        *)
            log_error "Cannot initialize DB to arch '$1': no prebuilt value"
            exit 1
            ;;
    esac

    pg_dump -c "$db" | psql --quiet palmed >/dev/null
    if is_db_clean ; then
        log_error "Badly loaded database: did not load benchmarks"
        exit 1
    fi
    log_info "Loaded database with prebuilt values for $1."
}

function init_state_checkpoints {
    # Initialize palmed checkpoints to some prebuilt data.
    # Arguments:
    # $1: arch (SKX, ZEN, …).

    case "$1" in
        "SKX")
            checkpoints="skx"
            ;;
        "ZEN")
            checkpoints="zen"
            ;;
        *)
            log_error "Cannot initialize checkpoints to arch '$1': no prebuilt value"
            exit 1
            ;;
    esac

    rm -rf "$HOME/palmed/checkpoints"
    mkdir -p "$HOME/palmed/checkpoints"
    cp -r "/palmed_prebuilt/checkpoints/$checkpoints/"* "$HOME/palmed/checkpoints"

    log_info "Loaded Palmed checkpoints with prebuilt values for $1."
}

function init_state_mapping {
    # Initialize the produced mapping to some prebuilt data.
    # Arguments:
    # $1: arch (SKX, ZEN, …).

    case "$1" in
        "SKX")
            mapping="scw-skx.2021-08-24.mapping"
            ;;
        "ZEN")
            mapping="scw-zen.2021-08-24.mapping"
            ;;
        *)
            log_error "Cannot initialize checkpoints to arch '$1': no prebuilt value"
            exit 1
            ;;
    esac

    rm -f "$HOME/palmed/out/mapping.pipedream.localhost"
    mkdir -p "$HOME/palmed/out"
    cp "/palmed_prebuilt/mapping/$mapping" "$HOME/palmed/out/mapping.pipedream.localhost"

    log_info "Loaded Palmed with prebuilt mapping from $1."
}

function init_state_eval_bench {
    # Initialize the evaluation benchmark files with some prebuilt data.
    # Arguments:
    # $1: arch (SKX, ZEN, …).

    case "$1" in
        "SKX")
            stem="scw-skx.2021-08-26.benchs"
            ;;
        "ZEN")
            stem="scw-zen.2021-08-24.benchs"
            ;;
        *)
            log_error "Cannot initialize evaluation benchmarks for arch '$1': no prebuilt value"
            exit 1
            ;;
    esac

    rm -f "$HOME/palmed/out/eval-"*.pickle
    mkdir -p "$HOME/palmed/out"
    for suffix in spec17 polybench; do
        cp -v "/palmed_prebuilt/bench_data/$stem.$suffix" \
            "$HOME/palmed/out/eval-$suffix.pickle"
    done

    log_info "Loaded Palmed with prebuilt evaluation benchmarks for $1."
}

function check_nostate {
    # Check that Palmed state is clean, or warn otherwise.
    # Args: any subset of
    # * db
    # * checkpoints
    # * mapping
    # * eval_bench
    # Will warn about all of those specified that are not clean

    warn_text=""

    while [ "$#" -gt 0 ]; do
        case "$1" in
            db)
                if ! is_db_clean; then
                    warn_text="$warn_text\n * database is not empty of measures"
                fi
                ;;
            checkpoints)
                if [ -d "$HOME/palmed/checkpoints/05_list_test_instructions.pipedream.localhost" ]; then
                    warn_text="$warn_text\n * palmed has populated checkpoints"
                fi
                ;;
            mapping)
                if [ -f "$HOME/palmed/out/mapping.pipedream.localhost" ]; then
                    warn_text="$warn_text\n * a mapping is already present"
                fi
                ;;
            eval_bench)
                # If any `eval-*.pickle` is present in out/
                if [ -d "$HOME/palmed/out" ] \
                    && find "$HOME/palmed/out" -iname 'eval-*.pickle' | grep -q . ; then
                    warn_text="$warn_text\n * some evaluation benchmarks are present"
                fi
                ;;
        esac
        shift
    done

    if [ -n "$warn_text" ]; then
        >&2 echo -e "${_BOLD}${_RED}WARNING: The following elements of " \
            "Palmed state are not clean:$warn_text\n" \
            "THIS MAY BE NORMAL if you already ran this script and " \
            "intentionally did not clean the state before running again. "\
            "It may also be the case that you forgot to clean the state with "\
            "palmed-reset_all_state.sh." "$_RESET"
        if ! prompt_bool "Continue without cleaning?"; then
            >&2 echo "Aborting."
            exit 1
        fi
    fi
}
