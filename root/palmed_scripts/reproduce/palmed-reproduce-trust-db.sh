#!/bin/bash

source $(dirname $(readlink -f "$0"))/_shared.sh

arch=$(find_arch $*) || exit 1

check_nostate 'checkpoints' 'mapping' 'eval_bench'

cd $HOME/palmed/
init_state_db "$arch"
generate_mapping "$arch"
evaluate_mapping "$arch"
aggregate_results
