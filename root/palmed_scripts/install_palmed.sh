#!/bin/bash

cd /home/palmed/palmed
source /home/palmed/venv/bin/activate

rm -f instructions.xml
wget --quiet https://uops.info/instructions.xml
pushd pipedream/tools/extract-xed-instruction-database
cmake .
make -j

instr_xed='../../src/pipedream/asm/x86/instructions_xed.py'
if [ "$SKIP_XED" -eq 0 ]; then
    ./extract-xed-instruction-database print-instr-db > "$instr_xed"
else
    echo -e "\e[1;31mWARNING: using prebuilt instructions_xed.py." \
        "For full reproduction, you should not set SKIP_XED.\e[0m"
    cp /palmed_prebuilt/instructions_xed.py "$instr_xed"
fi
popd

pip install -U pip
pip install setuptools wheel
pip install -r requirements.txt
pip install 'pipedream/'
pip install -e .
