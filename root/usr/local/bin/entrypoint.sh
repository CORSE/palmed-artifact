#!/bin/bash

cols=$(tput cols)
_last_status=""

CRED="\e[1;31m"
CGREEN="\e[1;32m"
CYELLOW="\e[1;33m"
CRESET="\e[0m"
_STATUS_PREFIX="$CYELLOW+$CRESET "

function disp_status {
    _last_status="$1"
    echo -ne "$_STATUS_PREFIX$_last_status"
}

function update_status {
    state_col=$(($cols - 8))
    echo -en "\r$_STATUS_PREFIX$_last_status"
    pad_cols=$(($state_col - $(echo -en "$_last_status" | wc -c) - 2)) 
    # 2: accounting for STATUS_PREFIX
    for i in $(seq 1 $pad_cols); do
        echo -n " "
    done
    if [ "$1" -ne 0 ]; then
        echo -ne "$CRED[FAIL]$CRESET"
    else
        echo -ne "$CGREEN  [OK]$CRESET"
    fi
    echo ""
}

function echo_error {
    >&2 echo -e "\e[1;31m" $* "\e[0m"
}

########## Increasing postgresql connections
sed -i "s/max_connections = .*$/max_connections = 300/g" \
        /etc/postgresql/13/main/postgresql.conf

########## Running supervisord
disp_status "Running supervisord..."
/usr/bin/supervisord --configuration /etc/supervisord.conf
update_status $?

disp_status "Waiting for Postgresql to be up..."
while ! pg_isready -q; do sleep 0.2; done
update_status 0

########## Initializing database
disp_status "Initializing palmed database..."
/usr/bin/sudo -u palmed /bin/bash -c \
    "cd /home/palmed/palmed \
    && /home/palmed/venv/bin/alembic --config /home/palmed/palmed/alembic.ini \
        upgrade head 2>/dev/null" > /dev/null
update_status "$?"

########## Checking for CAP_SYS_NICE
disp_status "Checking for CAP_SYS_NICE..."
/home/palmed/venv/bin/python /usr/local/bin/check_cap_sys_nice.py
rc=$?
if [ "$rc" -ne 0 ]; then
    update_status "$rc"
    echo_error "CAP_SYS_NICE not allowed; did you pass \`--cap-add=cap_sys_nice\` to docker run?"
    exit 1
else
    update_status 0
fi

########## Checking for PAPI & perf_event_paranoid
disp_status "Checking perf_event_paranoid and PAPI..."
/usr/local/bin/check-papi
rc=$?
if [ "$rc" -ne 0 ]; then
    update_status "$rc"
    echo_error "PAPI did not run correctly. " \
        "Did you set /proc/sys/kernel/perf_event_paranoid to 1 or lower?"
    exit 1
else
    update_status 0
fi

########## Registering Gurobi license key
disp_status "Registering Gurobi license key..."
if [ -z "$GUROBI_KEY" ] ; then
    update_status 1
    echo_error "ERROR: environment variable GUROBI_KEY is not set. Pass it to --env in docker run. Aborting."
    exit 1
elif [ "$GUROBI_KEY" = "SKIP" ]; then
    update_status 1
    echo_error "WARNING: skipping Gurobi configuration. Palmed won't work."
else
    /usr/bin/sudo -u palmed \
        /usr/bin/grbgetkey --quiet "$GUROBI_KEY"
    update_status $?
fi

########## Registering machine in Palmed database
disp_status "Registering machine in local Palmed database..."
echo -e "\n\n\n\n\n\n\n\n\ny\n" | /usr/bin/sudo -u palmed \
    /home/palmed/venv/bin/palmed-machines add > /dev/null
update_status "$?"

########## Creating DB restore point
disp_status "Creating a DB restoration point..."
/usr/bin/sudo -u palmed \
    pg_dump -c --file /home/palmed/.initial_db.sql palmed
update_status "$?"

########## Substituting hostname in prebuilt DBs
/usr/bin/sudo -u postgres psql --quiet palmed_prefill_skx \
    -c "UPDATE machine SET hostname = '$(hostname)' WHERE id = 1;"
/usr/bin/sudo -u postgres psql --quiet palmed_prefill_zen \
    -c "UPDATE machine SET hostname = '$(hostname)' WHERE id = 1;"


########## Binding consistent cgroup
disp_status "Checking privileged mode..."
mkdir -p /tmp/consistency
echo '0::/init.scope' > /tmp/consistency/cgroup
mount -o ro,bind /tmp/consistency/cgroup /proc/1/cgroup 2>/dev/null
rc=$?
if [ "$rc" -ne 0 ]; then
    update_status "$rc"
    echo_error "Docker is not running in privileged mode."
        "Did you pass --privileged to docker run?"
    exit 1
fi
update_status 0

########## Dropping privileges and providing a shell
export TERM=xterm-256color
cd ~palmed
exec setpriv \
    --reuid=palmed --regid=palmed --init-groups \
    --inh-caps=-all --reset-env \
    $SHELL
