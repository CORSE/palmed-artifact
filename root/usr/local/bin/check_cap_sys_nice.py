#!/usr/bin/env python3
import os
import sys

if __name__ == "__main__":
    try:
        os.sched_setscheduler(
            0, os.SCHED_FIFO, os.sched_param(os.sched_get_priority_max(os.SCHED_FIFO))
        )
    except PermissionError:
        print("Lacks capability: CAP_SYS_NICE")
        sys.exit(1)
