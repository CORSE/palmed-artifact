# Palmed artifact

This artifact is mainly meant to provide means for a third party to reproduce
Palmed results, as claimed in CGO'22.

It can also be used as an example for a simple working example of Palmed setup,
to use it on your own, or simply toy around with the project.

**Beware!** Palmed will run heavily on your machine, most likely for a few
hours.

**Also beware!** The quality of the results you can expect from Palmed greatly
depends on the quality of the data you gather from microbenchmarks. Hence,
great care should be taken to ensure a clean working environment; running in a
virtual machine, for instance, just won't do. Disabling hyperthreading is
highly recommended; disabling turboboost is a plus. Running other heavy
workloads in parallel *should* be fine, since Palmed takes hold of a core while
running a benchmark (scheduler mode FIFO, preventing context switches).

## Required experimental setup

Although this artifact is provided as a Docker, and is intended to run on any
computer with a Linux system, Palmed will only run (for now) on x86-64
architectures.

Given the computation intensity required, we **strongly advise** to run this
artifact on a dedicated machine (computation server, …) if available. Else,
your computer will not be normally usable for any other task for a few hours
(depending on your number of cores). We also advise aiming for a computer with
many cores.

**Given the level of precision in measurements required, you should not run
Palmed in a virtual machine, including VPSs.** It *will* work, but it is
expected to produce poor results.

Palmed will produce a model on any x86-64 architecture, but if you are willing
to reproduce our results, you should aim for a setup as close as possible to
ours, since the produced model is not expected to be the same on different
processors. At the very least, you should run Palmed on an Intel Skylake-SP
(SKL-SP) or AMD ZEN CPU, selecting the microarchitecture accordingly later (SKX
or ZEN).

If you are willing to use the exact same hardware as we did, our experiments
were run on respectively the `HC-BM1-XS` and `HM-BM1-M` offers from
[Scaleway](https://scaleway.com/), with respective CPUs Intel Xeon Silver 4114
and AMD EPYC 7401P. We are not affiliated in any respect with Scaleway.

In our experimental conditions, we did disable hyperthreading, but we did not
disable turboboost, since we did not notice any significative difference while
doing so.

## Run the Docker

You need to have [Docker](https://docker.com) installed on your system, with
the Docker daemon started -- this is well documented for most distributions.

### Configuring your system

For Palmed to work, it is necessary to configure your **host** system by:

* setting `perf_event_paranoid` to `1`:
  `echo 1 | sudo tee /proc/sys/kernel/perf_event_paranoid`.
  This is required to capture the number of ellapsed CPU cycles.
* disabling hyperthreading on your CPU for more accurate measurements. This
  *will* impact your machine's performance; a reboot will restore it to its
  normal state. Run `disable_hyperthreading.sh` to do so.

### Building the image

```bash
$ docker build -t palmed .
```

You can also pass `--build-arg SKIP_XED=1` to use our pre-built
`instructions_xed.py`. Although this does not *fully* reproduce our setup, it
saves about 15 minutes of build time, and is convenient for testing.

### Running the container

```bash
$ docker run --init --privileged --shm-size=1g --cap-add=cap_sys_nice --security-opt seccomp=seccomp.json -it --env GUROBI_KEY=[...] --name=palmed palmed
```

Notes on the parameters:

* It is **necessary** to run the Docker container with the capability
  `cap_sys_nice` enabled (`--cap-add=cap_sys_nice`); else, Palmed won't be able
  to set the scheduler to FIFO mode and pin processes to cores while running
  benchmarks.
* The `--security-opt seccomp=seccomp.json` changes which syscalls are blocked.
  Our file is based on [the default as of
  now](https://github.com/moby/moby/blob/df7bba7dbc6658ba48851eb4819e0a0a9508b18f/profiles/seccomp/default.json),
  with the `perf_event_open` syscall allowed (don't trust us, diff the files).
  This is required to use `libpapi` and read the CPU cycles ellapsed.
* For the same reasons of lower-level access, it is necessary to pass the
  `--privileged` flag.
* The `--shm-size=1g` increases the shared memory available to the container.
  Without this, requests to the database will fail.
* You must provide a Gurobi license key through the `GUROBI_KEY` environment
  variable, which can be obtained
  [here](https://www.gurobi.com/downloads/end-user-license-agreement-academic/)
  for academic purposes. If you would rather skip this step, you can pass
  the value `SKIP`, but Palmed will not work unless you manually register your
  license later.


You will be dropped to an unprivileged shell, from which you can run and
reproduce the experiments.

### Attaching a privileged shell

If you need a privileged shell, for some reason (reading log files, inspecting
parts of the system, installing packages, …), *while the Docker is running*,
run on your host:
```bash
$ docker exec -it palmed /bin/bash
```

### Restarting the docker

If you stopped the `docker run` process, and want to resume experimenting
without starting from scratch, run
```bash
$ docker start -at palmed
```

or start over after deleting the container:
```bash
$ docker rm palmed
```

## Relevant files and data

The docker container drops you as user `palmed` in a bash shell, in
`/home/palmed`. This home directory contains a virtualenv `venv`, in which the
relevant Python dependencies are installed, and the Palmed source code (and
some dependencies) in `palmed`.

Under `/palmed_prebuilt`, intermediary and result files can be found, along
with our `polybench` test suite (SPEC is not included for licensing reasons).

Palmed also relies on a database (here, postgresql) to store its benchmark
results. The database can be accessed through the `psql` command. Three
databases are initialized:

* `palmed`: empty DB, except for basic configuration;
* `palmed_prefill_skx`: DB filled with our experiments from our SKX (Intel)
  run;
* `palmed_prefill_zen`: DB filled with our experiments from our ZEN (AMD) run.

## Reproducing the experiments

First, ensure that you have a working Docker container (see above).

This artifact enables you to reproduce our experiments to various extents:

* `only-aggregate`: take for granted the produced model and evaluation
  benchmarks, simply aggregate the results and compute metrics;
* `only-evaluate`: take for granted the produced model, but re-evaluate it on
  basic blocks and derive metrics;
* `trust-db`: take for granted the produced microbenchmarks, but derive a model
  from those.  **BEWARE!** Palmed's model derivation algorithm can ask for more
  benchmarks at all times. Given that Gurobi (our solver) is not deterministic,
  even though Palmed completed once with those exact provided microbenchmarks,
  there is no guarantee that Palmed will not compute more benchmarks anyway;
* `reproduce-everything`: re-compute everything from scratch.

Given that we provide all our intermediary results and checkpoints, you should
be able to combine those options or reproduce from any other intermediary
points, if you're willing to dig slightly deeper in Palmed's architecture.

We provide helper scripts for those four options. We also provide below
instructions to aggregate results from evaluation benchmarks by hand, for a
deeper analysis; this can be done regardless of the option chosen, given the
script ran until the end — hence generating those evaluation benchmarks.

**BEWARE!** Palmed *caches* most of its results, to avoid recomputing the same
things all over again every time. If you want to switch from a lighter to a
heavier reproduction, you should first **clear all cache** by calling the
script `palmed-reset_all_state.sh`. Alternatively, you can spawn another Docker
container from the same image for this new experiment, to ensure your previous
computations won't alter this experiment.

### Only aggregate

Run the script
```bash
palmed-reproduce-only-aggregate.sh [ARCH]
```
where `[ARCH]` is `SKX` or `ZEN`.

This should print the main evaluation metrics, recomputed from the evaluation
benchmarks' results.

### Only evaluate

Run the script
```bash
palmed-reproduce-only-evaluate.sh [ARCH]
```
where `[ARCH]` is `SKX` or `ZEN`.

This should, given our pre-computed model, run the evaluation benchmarks, then
print the main evaluation metrics.

### Trust the DB

Run the script
```bash
palmed-reproduce-trust-db.sh [ARCH]
```
where `[ARCH]` is `SKX` or `ZEN`.

This should, given our pre-computed microbenchmarks, derive a model. This will
most probably compute some extra microbenchmarks (see above). It will then
proceed as for the lighter options above, running the evaluation benchmarks,
then printing the main evaluation metrics.

This will most likely run a few hours.

The first step of running Palmed from the beginning is to list and test
available instructions; this step will print a good quantity of warning for
unsupported instructions. These are normal -- control flow instructions, etc --
and can be safely ignored, a substantial amount of instructions should remain
in the end.

### Reproduce everything

Run the script
```bash
palmed-reproduce-everything.sh [ARCH]
```
where `[ARCH]` is `SKX` or `ZEN`.

This should start Palmed from scratch, running microbenchmarks and deriving a
model. Although the computation time depends heavily on your machine, this
should take a few hours, using up pretty much all CPU resources.
It will then proceed as for the lighter options above, running the evaluation
benchmarks, then printing the main evaluation metrics.

This will most likely run a few hours.

The first step of running Palmed from the beginning is to list and test
available instructions; this step will print a good quantity of warning for
unsupported instructions. These are normal -- control flow instructions, etc --
and can be safely ignored, a substantial amount of instructions should remain
in the end.

### Evaluating by hand

After running any of the options above, it is possible to explore further the
results.

The above commands will generate a file named
`/home/palmed/palmed/out/eval-[suite].pickle`, where `[suite]` is a test suite,
eg. `polybench`.

You can then analyze those evaluation files with the
`palmed-analyze-benchmarks [OPTIONS] out/eval-[suite].pickle` command; the
`--help` text should be self-explanatory.

This tool can output the metrics we provided in our paper, including RMS error
and Kendall's tau coefficient. It can further output the MAPE error (as
produced by PMEvo's paper). It is also used to generate the heatmaps as
produced in our paper.

## Licenses

The vast majority of Palmed dependencies are FOSS. Some dependencies, however,
are not:

* [Gurobi](https://www.gurobi.com/), the LP solver used, is proprietary.
  However, [free academic
  licenses](https://www.gurobi.com/academia/academic-program-and-licenses/) can
  be obtained.

* [Intel Architecture Code Analyzer
  (IACA)](https://www.intel.com/content/www/us/en/developer/articles/tool/architecture-code-analyzer.html),
  one of our related works against which Palmed is evaluated, is proprietary.
  However, as of 2021, Intel offers [free
  download](https://www.intel.com/content/www/us/en/developer/articles/tool/architecture-code-analyzer-download.html)
  of the tool.

* [The SPEC CPU 2017](https://www.spec.org/cpu2017/) benchmark suite, a
  test suite from which part of our evaluation is derived, is proprietary and
  paid; hence, it is not available in this artifact.


## Standalone evaluation (without artifact)

This Docker-based artifact is provided for the conveninece of reviewers, users
and curious folks who would want to try Palmed without installing it on their
machine.

However, should you want to install it outside of Docker, Palmed is
provided in [this Git repository](https://gitlab.inria.fr/nderumig/palmed/) --
see the `Dockerfile` for the exact frozen revision, or use the version from
`master` if you want the most recent one. The project's `README.md` should
provide you with enough documentation to run the project.

## Further documentation

The present file aims at documenting the artifact for Palmed, as submitted to
CGO'22. Although it is slightly out of this document's scope, it also documents
Palmed to some extent, and the various scripts in this archive could help you
to understand how everything works.

The main documentation for Palmed is its `README.md` file, present in [Palmed's
own repository](https://gitlab.inria.fr/nderumig/palmed/). The main `palmed`
and the various `palmed-…` sub-commands also (usually) feature an extensive
`--help` text. Please refer to both those sources if you feel some aspect is
lacking documentation. If all of this falls short, please contact the authors.
