FROM debian:bullseye

ARG SKIP_XED=0

# Install dependencies
RUN apt-get --assume-yes update
RUN apt-get --assume-yes install \
    python3 git postgresql supervisor bash-completion htop sudo curl wget \
    build-essential llvm unzip \
    vim emacs \
    libbz2-dev zlib1g-dev libsqlite3-dev libreadline-dev libssl-dev openssl \
    libpapi-dev papi-tools libcap2-bin cmake graphviz libgraphviz-dev
COPY pkgs/deb/ pkgs
RUN dpkg -i /pkgs/gurobi.deb

# Install iaca
RUN mkdir -p /tmp/iaca && \
    cd /tmp/iaca \
    && curl -s \
        'https://www.intel.com/content/dam/develop/external/us/en/protected/iaca-version-v3.0-lin64.zip' \
        -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0' \
        -o iaca.zip \
        > /dev/null \
    && unzip iaca.zip \
    && cp iaca-lin64/iaca /usr/local/bin/iaca \
    && chmod +x /usr/local/bin/iaca \
    && rm -rf /tmp/iaca

# Create an unprivileged user for experiments
RUN /usr/sbin/useradd -s /bin/bash -m palmed

# Install Python 3.8.6 ; create virtualenv
USER palmed:palmed
RUN \
    git clone --no-checkout --depth=1 --branch=v2.2.0 --quiet \
        https://github.com/pyenv/pyenv.git /home/palmed/.pyenv \
    && cd /home/palmed/.pyenv \
    && git checkout --quiet v2.2.0
RUN echo 'export PYENV_ROOT="$HOME/.pyenv"\n\
    export PATH="$PYENV_ROOT/bin:$PATH"\n\
    eval "$(pyenv init --path)"\n\
    eval "$(pyenv init -)"' >> /home/palmed/.bashrc
RUN /bin/bash --login -i -c 'pyenv install 3.8.6'
RUN /home/palmed/.pyenv/versions/3.8.6/bin/python3 -m venv /home/palmed/venv
RUN echo 'source "$HOME/venv/bin/activate"' >> /home/palmed/.bashrc

# Install palmed and deps
RUN git clone --quiet --recurse-submodules \
        https://gitlab.inria.fr/nderumig/palmed.git \
        /home/palmed/palmed \
    && cd /home/palmed/palmed \
    && git checkout --quiet '5ddd556b5a527b2ca406d87d3f9fa7081a954a64'
USER root:root
COPY root/palmed_scripts/install_palmed.sh /palmed_scripts/install_palmed.sh
COPY root/palmed_prebuilt /palmed_prebuilt
USER palmed:palmed
RUN /palmed_scripts/install_palmed.sh

# Copy all relevant static files
USER root:root
COPY root/ /

RUN echo 'export PATH="/palmed_scripts/reproduce:$PATH"' >> /home/palmed/.bashrc

# Compile check-papi
RUN gcc /src/check-papi.c -lpapi -o /usr/local/bin/check-papi

# Compile locales
RUN locale-gen

# Create Palmed database
RUN \
    /usr/bin/supervisord --configuration /etc/supervisord.conf \
    && while ! pg_isready -q; do sleep 0.2; done \
    && /usr/bin/sudo -u postgres psql --quiet \
        -c 'CREATE ROLE palmed WITH LOGIN' \
        -c 'CREATE DATABASE palmed OWNER palmed' \
        -c 'GRANT ALL PRIVILEGES ON DATABASE palmed TO palmed' \
        -c 'CREATE DATABASE palmed_prefill_skx OWNER palmed' \
        -c 'GRANT ALL PRIVILEGES ON DATABASE palmed_prefill_skx TO palmed' \
        -c 'CREATE DATABASE palmed_prefill_zen OWNER palmed' \
        -c 'GRANT ALL PRIVILEGES ON DATABASE palmed_prefill_zen TO palmed' \
        > /dev/null \
    && zcat /palmed_prebuilt/db/db-skx.sql.gz \
        | /usr/bin/sudo -u postgres psql --quiet palmed_prefill_skx \
    && zcat /palmed_prebuilt/db/db-zen.sql.gz \
        | /usr/bin/sudo -u postgres psql --quiet palmed_prefill_zen

# Grant CAP_SYS_NICE to python
# Do **NOT** move earlier -- docker-build doesn't have CAP_SYS_NICE, but
# docker-run has.
USER root:root
RUN setcap "CAP_SYS_NICE=+eip" /home/palmed/.pyenv/versions/3.8.6/bin/python3.8

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
